package com.utn.response.user;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by kano1 on 25/11/2016.
 */
public class UserPublicResponseWrapper {

    @JsonProperty("name")
    String name;

    @JsonProperty("lastName")
    String lastName;

    @JsonProperty("userName")
    String userName;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
