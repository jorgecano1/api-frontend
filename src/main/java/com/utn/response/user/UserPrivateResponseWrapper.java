package com.utn.response.user;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by kano1 on 25/11/2016.
 */
public class UserPrivateResponseWrapper {
    @JsonProperty("id")
    int id;
    @JsonProperty("username")
    String username;
    @JsonProperty("name")
    String name;
    @JsonProperty("lastName")
    String lastName;
    @JsonProperty("direction")
    String direction;
    @JsonProperty("phone")
    String phone;
    @JsonProperty("alternativeMail")
    String alternativeMail;
    @JsonProperty("city")
    String city;
    @JsonProperty("provinces")
    String provinces;
    @JsonProperty("country")
    String country;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAlternativeMail() {
        return alternativeMail;
    }

    public void setAlternativeMail(String alternativeMail) {
        this.alternativeMail = alternativeMail;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvinces() {
        return provinces;
    }

    public void setProvinces(String provinces) {
        this.provinces = provinces;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
