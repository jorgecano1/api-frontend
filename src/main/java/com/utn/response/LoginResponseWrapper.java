package com.utn.response;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by kano1 on 16/11/2016.
 */
public class LoginResponseWrapper {

    @JsonProperty
    String sessionId;

    public LoginResponseWrapper () {

    }

    public LoginResponseWrapper(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }
}
