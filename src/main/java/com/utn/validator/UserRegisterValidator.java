package com.utn.validator;

import com.utn.response.user.UserRegisterResponseWrapper;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Created by kano1 on 9/12/2016.
 */
@Component
public class UserRegisterValidator implements Validator {

    @Override
    public boolean supports(Class<?> aClass) {
        return UserRegisterResponseWrapper.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        UserRegisterResponseWrapper user = (UserRegisterResponseWrapper) o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username", "NotEmpty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "NotEmpty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "NotEmpty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lastName", "NotEmpty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "direction", "NotEmpty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "phone", "NotEmpty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "alternativeMail", "NotEmpty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "city", "NotEmpty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "provinces", "NotEmpty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "country", "NotEmpty");
    }
}
