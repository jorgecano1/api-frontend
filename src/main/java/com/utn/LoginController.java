package com.utn;

import com.utn.response.LoginResponseWrapper;
import com.utn.response.user.UserRegisterResponseWrapper;
import com.utn.validator.UserRegisterValidator;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kano1 on 8/12/2016.
 */
@Controller
public class LoginController {

    @Autowired
    private UserRegisterValidator userValidator;

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login (Model model) {
        return "login";
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String login (HttpSession session, HttpServletRequest request, Model model) {
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setContentType(MediaType.MULTIPART_FORM_DATA);

        MultiValueMap<String, Object> parts = new LinkedMultiValueMap<String, Object>();
        parts.add("user", request.getParameter("user"));
        parts.add("password", request.getParameter("password"));
        parts.add("isRemember", 1);

        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<MultiValueMap<String, Object>>(
                parts, requestHeaders);

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new FormHttpMessageConverter());
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

        //LoginResponseWrapper session = restTemplate.postForObject("http://localhost:8080/login", requestEntity, LoginResponseWrapper.class);
        try {
            ResponseEntity<LoginResponseWrapper> responseEntity = restTemplate.exchange("http://localhost:8080/login", HttpMethod.POST, requestEntity, LoginResponseWrapper.class);

            if (responseEntity.getStatusCode() == HttpStatus.OK) {
                // guardo como variable de session el Id y el usuario
                session.setAttribute("sessionId", responseEntity.getBody().getSessionId());
                session.setAttribute("userName", request.getParameter("user"));
                return ("redirect:/inbox");
            }
        } catch (final HttpClientErrorException e) {
            if (e.getStatusCode() == HttpStatus.UNPROCESSABLE_ENTITY) {
                model.addAttribute("error", "User or password Invalidate!");
                return ("login");
            }
        }

        return ("/error");
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logout (HttpSession session, Model model){
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.add("sessionId", (String) session.getAttribute("sessionId"));

        HttpEntity requestEntity = new HttpEntity(requestHeaders);

        RestTemplate restTemplate = new RestTemplate();

        ResponseEntity responseEntity = restTemplate.exchange("http://localhost:8080/logout", HttpMethod.GET, requestEntity, Object.class);

        if (responseEntity.getStatusCode() == HttpStatus.OK) {
            session.removeAttribute("sessionId");
        }

        return "redirect:/login";
    }

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String register(Model model){
        model.addAttribute("userForm", new UserRegisterResponseWrapper());
        return "register";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String register(@ModelAttribute("userForm") UserRegisterResponseWrapper userForm, BindingResult bindingResult, Model model,
                           RedirectAttributes redirectAttributes){
        userValidator.validate(userForm, bindingResult);

        if (bindingResult.hasErrors()) {
            return "register";
        }

        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<UserRegisterResponseWrapper> requestEntity = new HttpEntity<UserRegisterResponseWrapper>(
                userForm, requestHeaders);

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new FormHttpMessageConverter());
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

        try {
            ResponseEntity responseEntity = restTemplate.exchange("http://localhost:8080/register", HttpMethod.POST, requestEntity, Object.class);

            if (responseEntity.getStatusCode() == HttpStatus.CREATED) {
                redirectAttributes.addFlashAttribute("message", "User created.");
                return "redirect:/login";
            }
        } catch (final HttpClientErrorException e) {
            if (e.getStatusCode() == HttpStatus.CONFLICT) {
                model.addAttribute("error", "Username exits.");
                return ("register");
            } else {
                model.addAttribute("error", "Error try again later");
                return ("register");
            }
        }

        return ("error");
    }

}
