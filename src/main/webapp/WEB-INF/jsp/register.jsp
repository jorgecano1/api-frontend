
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html lang="en">
<body>
    <form:form action="/register" modelAttribute="userForm" method="post">
        <spring:bind path="username">
            <form:input type="text" path="username" placeholder="Username" autofocus="true"></form:input>
            <form:errors path="username"></form:errors>
        </spring:bind>
        <spring:bind path="password">
            <form:input type="text" path="password" placeholder="Password"></form:input>
            <form:errors path="password"></form:errors>
        </spring:bind>
        <spring:bind path="name">
            <form:input type="text" path="name" placeholder="Name"></form:input>
            <form:errors path="name"></form:errors>
        </spring:bind>
        <spring:bind path="lastName">
            <form:input type="text" path="lastName" placeholder="LastName"></form:input>
            <form:errors path="lastName"></form:errors>
        </spring:bind>
        <spring:bind path="direction">
            <form:input type="text" path="direction" placeholder="Direction"></form:input>
            <form:errors path="direction"></form:errors>
        </spring:bind>
        <spring:bind path="phone">
            <form:input type="text" path="phone" placeholder="Phone"></form:input>
            <form:errors path="phone"></form:errors>
        </spring:bind>
        <spring:bind path="alternativeMail">
            <form:input type="text" path="alternativeMail" placeholder="Alternative Email"></form:input>
            <form:errors path="alternativeMail"></form:errors>
        </spring:bind>
        <spring:bind path="city">
            <form:input type="text" path="city" placeholder="City"></form:input>
            <form:errors path="city"></form:errors>
        </spring:bind>
        <spring:bind path="provinces">
            <form:input type="text" path="provinces" placeholder="Provinces" ></form:input>
            <form:errors path="provinces"></form:errors>
        </spring:bind>
        <spring:bind path="country">
            <form:input type="text" path="country" placeholder="Country"></form:input>
            <form:errors path="country"></form:errors>
        </spring:bind>

        <span>${error}</span>
        <button type="submit">Submit</button>
    </form:form>
</body>
</html>