<html>
<body>
    <form action="/login" method="post">
        <h2>Log in</h2>
        <span>${message}</span>

        <input name="user" type="text" placeholder="Username" autofocus="true"/>
        <input name="password" type="password" placeholder="Password"/>
        <span>${error}</span>

        <button type="submit">Log In</button>
        <h4><a href="/register">Create an account</a></h4>
    </form>
</body>
</html>
